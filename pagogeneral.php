<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista de Pagos</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Secular+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
</head>
<style>
    body {
        background-color: #88DFE2;
    }

    div.container {
        background-color: #FAF8ED;
        border-radius: 10px;
    }

    td {
        font-family: 'Ubuntu', sans-serif;
    }

    a {
        color: white;
    }

    h1 {
        font-family: 'Secular One', sans-serif;
    }
</style>
<body>
    <div class="container pt-2 mt-4 pb-4">
        <h1>Lista de Pagos</h1>
        <hr>
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th>ID Pago</th>
                    <th>ID Socio</th>
                    <th>Monto</th>
                    <th>Fecha</th>
                </tr>
            </thead>
            <tbody>
                <?php
                // Realizar la conexión a la base de datos (ajusta los valores según tu configuración)
                include("conexion.php");

                // Verificar la conexión
                if ($conn->connect_error) {
                    die("Error en la conexión a la base de datos: " . $conn->connect_error);
                }

                // Consulta SQL para buscar todos los pagos ordenados por id_pago descendente
                $sql = "SELECT * FROM pago ORDER BY id_pago DESC"; //consulta en orden DESC descendente
                $resultado = $conn->query($sql);

                if ($resultado->num_rows > 0) {
                    // Se encontraron pagos
                    while ($fila = $resultado->fetch_assoc()) {
                        echo "<tr>";
                        echo "<td>" . $fila["id_pago"] . "</td>";
                        echo "<td>" . $fila["id_socio"] . "</td>";
                        echo "<td> $" . $fila["monto"] . "</td>";
                        echo "<td>" . $fila["fecha"] . "</td>";
                        echo "</tr>";
                    }
                    
                } else {
                    // No se encontraron pagos
                    echo "<tr><td colspan='4'>No se encontraron pagos</td></tr>";
                }

                // Cerrar la conexión a la base de datos
                $conn->close();
                ?>
            </tbody>
            
        </table>
        <a href="pagos.html" class="btn btn-secondary">Regresar</a>
    </div>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>

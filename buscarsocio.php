<?php
include("conexion.php");
if ($conn->connect_error) {
    echo ("Error en la conexión a la base de datos: " . $conn->connect_error);
}

$id_socio = $_POST["id_socio"];

$sql = "SELECT * FROM socios WHERE id_socio = $id_socio";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // El socio fue encontrado, obtén los datos
    $row = $result->fetch_assoc();
    $nombre = $row["nombre"];
    $apellidop = $row["apellidop"];
    $apellidom = $row["apellidom"];
    $direccion = $row["direccion"];
    $telefono = $row["telefono"];
    $correo = $row["correo"];
    $fecha_registro = $row["fecha"];
} else {
    // El socio no fue encontrado
    $error = "El socio no existe.";
}

$conn->close();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Secular+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
    <title>Busqueda Socio</title>
</head>
<style>
    body {
        background-color: #88DFE2;
    }

    div.card {
        background-color: #FAF8ED;
        border-radius: 10px;
    }
    h2{
        font-family: 'Secular One', sans-serif;
    }
    li{
        font-family: 'Ubuntu', sans-serif;
    }
</style>

<body>
    <?php if (isset($nombre)) : ?>
        <div class="container mt-5">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">Información del socio:</h2>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><strong>Nombre:</strong> <?php echo $nombre; ?></li>
                        <li class="list-group-item"><strong>Apellido Paterno:</strong> <?php echo $apellidop; ?></li>
                        <li class="list-group-item"><strong>Apellido Materno:</strong> <?php echo $apellidom; ?></li>
                        <li class="list-group-item"><strong>Dirección:</strong> <?php echo $direccion; ?></li>
                        <li class="list-group-item"><strong>Teléfono:</strong> <?php echo $telefono; ?></li>
                        <li class="list-group-item"><strong>Correo:</strong> <?php echo $correo; ?></li>
                        <li class="list-group-item"><strong>Fecha de Registro:</strong> <?php echo $fecha_registro; ?></li>
                    </ul>
                    <a href="principal.php" class="btn btn-secondary mt-3">Salir</a>
                </div>
            </div>
        </div>
    <?php elseif (isset($error)) : ?>
        <div class="container mt-5 text-center">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
                <br></br>
                <a href="principal.php" class="btn btn-secondary">Salir</a>
            </div>
        </div>
    <?php endif; ?>

</body>

</html>
<?php
// Obtener los valores de los campos del formulario
$id_socio = $_POST["id_socio"];
$monto = $_POST["monto"];
$fecha = $_POST["fecha"];

// Validar que los campos no estén vacíos
if (empty($id_socio) || empty($monto) || empty($fecha)) {
    echo "Por favor, completa todos los campos";
    // Redirigir a la página de creación de pago
    header("Location: registropago.html");
    exit();
}

// Realizar la conexión a la base de datos (ajusta los valores según tu configuración)
include("conexion.php");

// Verificar la conexión
if ($conn->connect_error) {
    die("Error en la conexión a la base de datos: " . $conn->connect_error);
}

// Consulta SQL para verificar si el socio existe
$sql_verificar = "SELECT * FROM socios WHERE id_socio = '$id_socio'";
$resultado = $conn->query($sql_verificar);

if ($resultado->num_rows == 0) {
    echo "El socio con ID $id_socio no existe";
    // Redirigir a la página de creación de pago
    header("Location: registropago.html");
    exit();
}

// Consulta SQL para insertar los datos en la tabla
$sql_insertar = "INSERT INTO pago (id_socio, monto, fecha)
                VALUES ('$id_socio', '$monto', '$fecha')";

if ($conn->query($sql_insertar) === TRUE) {
    echo "Datos insertados correctamente";
    // Redirigir a la página principal
    header("Location: pagos.html");
} else {
    echo "Error al insertar datos: " . $conn->error;
    // Redirigir a la página de creación de pago
    header("Location: registropago.html");
}

// Cerrar la conexión a la base de datos
$conn->close();
?>

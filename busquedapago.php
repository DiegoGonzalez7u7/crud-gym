<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Secular+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
    <title>Busqueda de pago</title>
</head>
<style>
    body {
        background-color: #88DFE2;
    }
    div.card-body{
        background-color: #FAF8ED;
        border-radius: 10px;
    }
    h2{
        font-family: 'Secular One', sans-serif;
    }
    h5{
        font-family: 'Ubuntu', sans-serif;
    }
    p{
        font-family: 'Ubuntu', sans-serif;
    }
</style>
<body>
<?php
// Obtener el ID del socio a buscar
$id_socio = $_POST["id_socio"];

// Validar que el campo no esté vacío
if (empty($id_socio)) {
    echo "Por favor, ingresa el ID del socio";
    // Redirigir a la página de búsqueda de pagos
    header("Location: buscarpagos.html");
    exit();
}

// Realizar la conexión a la base de datos (ajusta los valores según tu configuración)
include("conexion.php");

// Verificar la conexión
if ($conn->connect_error) {
    die("Error en la conexión a la base de datos: " . $conn->connect_error);
}

// Consulta SQL para obtener el nombre del socio
$sql_socio = "SELECT nombre FROM socios WHERE id_socio = '$id_socio'";
$resultado_socio = $conn->query($sql_socio);

if ($resultado_socio->num_rows > 0) {
    // Se encontró el socio
    $fila_socio = $resultado_socio->fetch_assoc();
    $nombre_socio = $fila_socio["nombre"];

    // Consulta SQL para buscar los pagos del socio especificado
    $sql_pagos = "SELECT * FROM pago WHERE id_socio = '$id_socio'";
    $resultado_pagos = $conn->query($sql_pagos);

    if ($resultado_pagos->num_rows > 0) {
        // Se encontraron pagos del socio
        echo '<div class="container mt-2">';
        echo '<h2>Pagos del socio: ' . $nombre_socio . '</h2>';
        echo '<hr>';
        
        while ($fila = $resultado_pagos->fetch_assoc()) {
            echo '<div class="card mb-4 mt-4">';
            echo '<div class="card-body">';
            echo '<h5 class="card-title">ID Pago: ' . $fila["id_pago"] . '</h5>';
            echo '<p class="card-text">Monto: ' . $fila["monto"] . '</p>';
            echo '<p class="card-text">Fecha: ' . $fila["fecha"] . '</p>';
            echo '</div>';
            echo '</div>';
        }
        echo '<a href="busquedapago.html" class="btn btn-secondary mb-3">Salir</a>';
        
        echo '</div>';
    } else {
        // No se encontraron pagos del socio
        echo '<div class="container mt-2 text-center">';
        echo '<h2>No se encontraron pagos para el socio: ' . $nombre_socio . '</h2>';
        echo '<a href="busquedapago.html" class="btn btn-secondary mt-4">Regresar</a>';
        echo '</div>';
    }
} else {
    // No se encontró el socio
    echo '<div class="container mt-2 text-center">';
    echo '<h2>No se encontró el socio con ID: ' . $id_socio . '</h2>';
    echo '<a href="busquedapago.html" class="btn btn-secondary mt-4">Regresar</a>';
    echo '</div>';
}

// Cerrar la conexión a la base de datos
$conn->close();
?>
</body>
</html>
<?php
// Obtener el ID del registro a eliminar
$id = $_POST['id']; // Suponiendo que pasas el ID a través de un formulario

// Realizar la conexión a la base de datos (ajusta los valores según tu configuración)
include("conexion.php");

// Verificar la conexión
if ($conn->connect_error) {
    die('Error de conexión a la base de datos: ' . $conn->connect_error);
}

// Verificar si el registro existe en la tabla "socios"
$consultaSocios = "SELECT * FROM socios WHERE id_socio = $id";
$resultadoSocios = $conn->query($consultaSocios);

if ($resultadoSocios->num_rows > 0) {
    $socio = $resultadoSocios->fetch_assoc();
    $nombreCliente = $socio['nombre']; // Obtener el nombre del cliente

    // El registro existe en la tabla "socios", verificar la relación en la tabla "pago"
    $consultaRelacion = "SELECT * FROM pago WHERE id_socio = $id";
    $resultadoRelacion = $conn->query($consultaRelacion);

    if ($resultadoRelacion->num_rows > 0) {
        // Existe una relación en la tabla "pago", no se puede eliminar
        $error = "No se puede eliminar el registro porque existen pagos del cliente: " . $nombreCliente;
    } else {
        // No hay relación en la tabla "pago", procedemos a eliminar el registro de la tabla "socios"

        // Preparar la consulta SQL para eliminar el registro
        $sql = "DELETE FROM socios WHERE id_socio = $id";

        // Ejecutar la consulta SQL
        if ($conn->query($sql) === TRUE) {
            // El registro se eliminó exitosamente
            header('Location: principal.php');
            exit();
        } else {
            // Ocurrió un error durante la eliminación
            echo 'Error al eliminar el registro: ' . $conn->error;
        }
    }
} else {
    // El registro no existe en la tabla "socios"
    $error = "El registro con el ID $id no existe en la tabla 'socios'.";
}

// Cerrar la conexión a la base de datos
$conn->close();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <title>No existe</title>
</head>
<style>
    body {
        background-color: #88DFE2;
    }
</style>

<body>
<?php if(isset($error)) : ?>
    <div class="container mt-5 text-center">
        <div class="alert alert-danger" role="alert">
            <?php echo $error; ?>
            <br></br>
            <a href="principal.php" class="btn btn-secondary">Salir</a>
        </div>
    </div>
<?php endif; ?>
</body>
</html>

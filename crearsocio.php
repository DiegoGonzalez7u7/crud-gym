<?php
// Obtener los valores de los campos del formulario
$id_socio = $_POST["id_socio"];
$nombre = $_POST["nombre"];
$apellidop = $_POST["apellidop"];
$apellidom = $_POST["apellidom"];
$direccion = $_POST["direccion"];
$telefono = $_POST["telefono"];
$correo = $_POST["correo"];
$fecha_registro = $_POST["fecha_registro"];

// Validar que los campos no estén vacíos
if (empty($id_socio) || empty($nombre) || empty($apellidop) || empty($apellidom) || empty($direccion) || empty($telefono) || empty($correo) || empty($fecha_registro)) {
    echo "Por favor, completa todos los campos";
    header("Location: crearsocio.html");
    exit();
}

include("conexion.php");
// Verificar si la conexión es exitosa
if ($conn->connect_error) {
    die("Error en la conexión a la base de datos: " . $conn->connect_error);
}

// Consulta previa para verificar si el socio ya existe
$sql_verificar = "SELECT id_socio FROM socios WHERE id_socio = '$id_socio'";
$resultado_verificar = $conn->query($sql_verificar);

if ($resultado_verificar->num_rows > 0) {
    echo "El socio ya existe";
    header("Location: crearsocio.html");
    exit();
}

// Consulta SQL para insertar los datos en la tabla
$sql = "INSERT INTO socios (id_socio, nombre, apellidop, apellidom, direccion, telefono, correo, fecha)
        VALUES ('$id_socio', '$nombre', '$apellidop', '$apellidom', '$direccion', '$telefono', '$correo', '$fecha_registro')";

if ($conn->query($sql) === TRUE) {
    echo "Datos insertados correctamente";
    header("Location: principal.php");
} else {
    echo "Error al insertar datos: " . $conn->error;
    header("Location: crearsocio.html");
}

$conn->close();
?>

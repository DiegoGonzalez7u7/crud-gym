<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gestión de socios - Gym</title>
    <!-- Estilos de Bootstrap 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Inconsolata&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
</head>
<style>
    body {
        background-color: #88DFE2;
    }

    div.card {
        background-color: #FAF8ED;
        border-radius: 10px;
    }

    h5 {
        font-family: 'Ubuntu', sans-serif;
    }

    a {
        color: white;
    }

    p {
        font-family: 'Inconsolata', monospace;
    }
</style>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto"> <!-- Contenedor para "Socios" y "Pagos" con la clase mr-auto -->
                <li class="navbar-brand mb-0 h1">
                    <a class="ml-3 mr-3 " href="principal.php">Socios</a>
                </li>
                <li class="navbar-brand mb-0 h1">
                    <a class="ml-3 mr-3 " href="pagos.html">Pagos</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto"> <!-- Contenedor para "Logout" con la clase ml-auto -->
                <li class="navbar-brand h1">
                    <a class="" href="logout.php">Logout</a>
                </li>
            </ul>
        </div>
    </nav>
    <?php
    session_start();

    // Verificar si el usuario ha iniciado sesión
    if (isset($_SESSION["nombre"])) {
        $nombre = $_SESSION["nombre"];
        // Aquí puedes realizar las acciones que necesites con el valor de $_SESSION["id_usuario"]
        echo "<h5 class='pt-2 text-center'>Bienvenido,<strong> $nombre</strong></h5>";
    } else {
        // El usuario no ha iniciado sesión, redirigirlo a la página de inicio de sesión
        header("Location: index.php");
        exit();
    }
    ?>

    <div class="container my-5">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-4 mb-3">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Crear socio</h5>
                        <p class="card-text">Agrega un nuevo socio a la BD.</p>
                        <a href="crearsocio.html" class="btn btn-primary">Crear</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-4 mb-3">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Buscar socio</h5>
                        <p class="card-text">Busca un socio en la BD.</p>
                        <a href="buscarsocio.html" class="btn btn-primary">Buscar</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-4 mb-3">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Editar socio</h5>
                        <p class="card-text">Edita los datos de un socio en la BD.</p>
                        <a href="editarsocio.html" class="btn btn-primary">Editar</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-4 mb-3">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Eliminar socio</h5>
                        <p class="card-text">Elimina un socio de la BD</p>
                        <a href="eliminarsocio.html" class="btn btn-danger">Eliminar</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 mb-3">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Busqueda general de socios</h5>
                        <p class="card-text">Busca a todos los socios</p>
                        <a href="busquedageneral.php" class="btn btn-info">Buscar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
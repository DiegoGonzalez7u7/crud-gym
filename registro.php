<?php
include("conexion.php");

// Verificar si la conexión es exitosa
if ($conn->connect_error) {
    die("Error en la conexión a la base de datos: " . $conn->connect_error);
}

// Procesar el formulario de registro
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id= $_POST["id"];
    $nombre = $_POST["nombre"];
    $contraseña = $_POST["pass"];
    $correo = $_POST["correo"];

    // Verificar si el usuario ya existe en la base de datos
    $sql_verificar = "SELECT * FROM sesion WHERE id_usuario = '$id'";
    $result_verificar = $conn->query($sql_verificar);

    if ($result_verificar->num_rows > 0) {
        $error = "El id de usuario ya está registrado";
        header("Location: registro.html");
    } else {
        // Insertar el nuevo usuario en la base de datos
        $sql_insertar = "INSERT INTO sesion (id_usuario, nombre_usuario,contraseña,correo) VALUES ('$id','$nombre', '$contraseña', '$correo')";

        if ($conn->query($sql_insertar) === true) {
            $success = "Registro exitoso";
            header("Location: index.html");
        } else {
            $error = "Error en el registro: " . $conn->error;
            header("Location: registro.html");
        }
    }
}

$conn->close();
?>


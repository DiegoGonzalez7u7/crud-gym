<?php
include("conexion.php");
if ($conn->connect_error) {
    echo ("Error en la conexión a la base de datos: " . $conn->connect_error);
}

$id_socio = $_POST["id_socio"];

$sql = "SELECT * FROM socios WHERE id_socio = $id_socio";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // El socio fue encontrado, obtén los datos
    $row = $result->fetch_assoc();
    $id = $row["id_socio"];
    $nombre = $row["nombre"];
    $apellidop = $row["apellidop"];
    $apellidom = $row["apellidom"];
    $direccion = $row["direccion"];
    $telefono = $row["telefono"];
    $correo = $row["correo"];
    $fecha_registro = $row["fecha"];
} else {
    // El socio no fue encontrado
    $error = "El socio no existe.";
}

$conn->close();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Secular+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
    <title>Busqueda Socio</title>
</head>
<style>
body{
    background-color: #88DFE2;
}
h2{
    font-family: 'Secular One', sans-serif;
}
label{
    font-family: 'Ubuntu', sans-serif;
}
div.card{
    background-color: #FAF8ED;
        border-radius: 10px;
}
</style>

<body>
    <?php if (isset($nombre)) : ?>
        <div class="container mt-5 mb-5">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">Información del socio:</h2>
                    <form action="editarsocio2.php" method="POST" onsubmit="return validarCampos()">
                        <div class="form-group">
                            <label for="nombre"><strong>ID:</strong></label>
                            <input type="number" class="form-control" id="id" value="<?php echo $id; ?>" name="id" readonly>
                        </div>
                        <div class="form-group">
                            <label for="nombre"><strong>Nombre:</strong></label>
                            <input type="text" class="form-control" id="nombre" value="<?php echo $nombre; ?>" name="nombre">
                        </div>
                        <div class="form-group">
                            <label for="apellidop"><strong>Apellido Paterno:</strong></label>
                            <input type="text" class="form-control" id="apellidop" value="<?php echo $apellidop; ?>" name="apellidop">
                        </div>
                        <div class="form-group">
                            <label for="apellidom"><strong>Apellido Materno:</strong></label>
                            <input type="text" class="form-control" id="apellidom" value="<?php echo $apellidom; ?>" name="apellidom">
                        </div>
                        <div class="form-group">
                            <label for="direccion"><strong>Dirección:</strong></label>
                            <input type="text" class="form-control" id="direccion" value="<?php echo $direccion; ?>" name="direccion">
                        </div>
                        <div class="form-group">
                            <label for="telefono"><strong>Teléfono:</strong></label>
                            <input type="tel" class="form-control" id="telefono" value="<?php echo $telefono; ?>" name="telefono">
                        </div>
                        <div class="form-group">
                            <label for="correo"><strong>Correo:</strong></label>
                            <input type="email" class="form-control" id="correo" value="<?php echo $correo; ?>" name="correo">
                        </div>
                        <div class="form-group">
                            <label for="fecha_registro"><strong>Fecha de Registro:</strong></label>
                            <input type="date" class="form-control" id="fecha_registro" value="<?php echo $fecha_registro; ?>" name="fecha_registro">
                        </div>
                        <button type="submit" class="btn btn-success">Actualizar</button>
                        <a href="editarsocio.html" class="btn btn-secondary ml-3">Salir</a>
                    </form>
                </div>
            </div>
        </div>
    <?php elseif (isset($error)) : ?>
        <div class="container mt-5 text-center">
        <div class="alert alert-danger" role="alert">
            <?php echo $error; ?>
            <br></br>
            <a href="principal.php" class="btn btn-secondary">Salir</a>
        </div>
    </div>
    <?php endif; ?>


</body>

</html>
<script>
    function validarCampos() {
        var id = document.getElementById("id").value;
        var nombre = document.getElementById("nombre").value;
        var apellidop = document.getElementById("apellidop").value;
        var apellidom = document.getElementById("apellidom").value;
        var direccion = document.getElementById("direccion").value;
        var correo = document.getElementById("correo").value;
        var fecha_registro = document.getElementById("fecha_registro").value;

        var telefonoInput = document.getElementById("telefono");
        var telefono = telefonoInput.value;

        // Validar el número de teléfono
        var telefonoRegex = /^[0-9]{10}$/;
        if (!telefonoRegex.test(telefono)) {
            alert("Por favor, ingresa un número de teléfono válido (10 dígitos).");
            return false; // Evita que el formulario se envíe
        }

        if (id.trim() === '') {
            alert("Por favor, ingresa un id.");
            return false; // Evita que el formulario se envíe
        }

        if (nombre.trim() === '') {
            alert("Por favor, ingresa un nombre.");
            return false; // Evita que el formulario se envíe
        }

        if (apellidop.trim() === '') {
            alert("Por favor, ingresa un apellido paterno.");
            return false; // Evita que el formulario se envíe
        }

        if (apellidom.trim() === '') {
            alert("Por favor, ingresa un apellido materno.");
            return false; // Evita que el formulario se envíe
        }

        if (direccion.trim() === '') {
            alert("Por favor, ingresa una dirección.");
            return false; // Evita que el formulario se envíe
        }

        if (correo.trim() === '') {
            alert("Por favor, ingresa un correo electrónico.");
            return false; // Evita que el formulario se envíe
        }

        if (fecha_registro.trim() === '') {
            alert("Por favor, ingresa una fecha de registro.");
            return false; // Evita que el formulario se envíe
        }

        // Todos los campos están completos
        return true;
    }
</script>
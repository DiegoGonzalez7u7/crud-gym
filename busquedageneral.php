<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Secular+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
    <title>Busqueda General</title>
</head>
<style>
    body {
        background-color: #88DFE2;
    }

    div.container {
        background-color: #FAF8ED;
        border-radius: 10px;
    }

    td {
        font-family: 'Ubuntu', sans-serif;
    }

    a {
        color: white;
    }

    h2 {
        font-family: 'Secular One', sans-serif;
    }
</style>

<body>
    <?php
    // Realizar la conexión a la base de datos (ajusta los valores según tu configuración)
    include("conexion.php");

    // Verificar la conexión
    if ($conn->connect_error) {
        die("Error en la conexión a la base de datos: " . $conn->connect_error);
    }

    // Consulta SQL para obtener todos los socios
    $sql = "SELECT * FROM socios";
    $resultado = $conn->query($sql);

    if ($resultado->num_rows > 0) {
        // Se encontraron socios
        echo '<h2 class="text-center mt-2">Lista de Socios</h2>';
        echo '<div class="container mt-3 pt-3">';
        echo '<table class="table">';
        echo '<thead class="thead-dark">';
        echo '<tr>';
        echo '<th>ID Socio</th>';
        echo '<th>Nombre</th>';
        echo '<th>Apellido paterno</th>';
        echo '<th>Apellido materno</th>';
        echo '<th>Direccion</th>';
        echo '<th>Telefono</th>';
        echo '<th>Correo</th>';
        echo '<th>Fecha</th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        while ($fila = $resultado->fetch_assoc()) {
            echo '<tr>';
            echo '<td>' . $fila["id_socio"] . '</td>';
            echo '<td>' . $fila["nombre"] . '</td>';
            echo '<td>' . $fila["apellidop"] . '</td>';
            echo '<td>' . $fila["apellidom"] . '</td>';
            echo '<td>' . $fila["direccion"] . '</td>';
            echo '<td>' . $fila["telefono"] . '</td>';
            echo '<td>' . $fila["correo"] . '</td>';
            echo '<td>' . $fila["fecha"] . '</td>';
            echo '</tr>';
        }

        echo '</tbody>';
        echo '</table>';
        echo '<a href="principal.php" class="btn btn-secondary mb-3">Regresar</a>';
        echo '</div>';
    } else {
        // No se encontraron socios
        echo "No se encontraron socios en la base de datos";
    }

    // Cerrar la conexión a la base de datos
    $conn->close();
    ?>

</body>

</html>
-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-05-2023 a las 23:03:35
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gym`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE `pago` (
  `id_pago` int(10) NOT NULL,
  `id_socio` int(10) NOT NULL,
  `monto` double NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='pagos de los socios relacionados con la tabla socios';

--
-- Volcado de datos para la tabla `pago`
--

INSERT INTO `pago` (`id_pago`, `id_socio`, `monto`, `fecha`) VALUES
(1, 19680159, 500, '2023-05-15'),
(2, 19680159, 1200, '2023-05-15'),
(3, 19680159, 1200, '2023-06-10'),
(4, 19680159, 7800, '2023-05-15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesion`
--

CREATE TABLE `sesion` (
  `id_usuario` varchar(10) NOT NULL,
  `nombre_usuario` varchar(50) NOT NULL,
  `contraseña` varchar(10) NOT NULL,
  `correo` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `sesion`
--

INSERT INTO `sesion` (`id_usuario`, `nombre_usuario`, `contraseña`, `correo`) VALUES
('', '', '', ''),
('1', 'Diego Gonzalez', 'diego100', 'killerclanec@gmail.com'),
('123', 'Merari Alvarez Aquino', 'diego100', 'meri@gmail.com'),
('19680105', 'Anibal Eliud', 'diego100', 'eliud@gmail.com'),
('19680159', 'Diego Noel Gonzalez Tamariz', 'diego100', '19680159@cuautla.tecnm.mx'),
('2', 'Juan', 'diego100', 'juan@gmail.com'),
('456', 'Gonzalez Tamariz', 'diego100', 'gzltm@gmail.com'),
('735593', 'GonzaUuU', 'diego100', 'gonzauuu@gmail.com'),
('777', 'pedro', 'diego100', 'pedor@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `socios`
--

CREATE TABLE `socios` (
  `id_socio` int(10) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellidop` varchar(40) NOT NULL,
  `apellidom` varchar(40) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` bigint(10) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='esta tabla contiene los datos de los socios ';

--
-- Volcado de datos para la tabla `socios`
--

INSERT INTO `socios` (`id_socio`, `nombre`, `apellidop`, `apellidom`, `direccion`, `telefono`, `correo`, `fecha`) VALUES
(321, 'monty', 'gonzalez', 'Tamariz', 'En la calle', 7358964152, 'juan@gmail.com', '2023-06-03'),
(456, 'Mera', 'Alva', 'Aquino', '5 de febrero', 7355938541, 'm@gmail.com', '2023-04-30'),
(735, 'Diego', 'Gonzalez ', 'Tamariz', 'Lomas Turbas', 7355934610, 'killerclanec@gmail.com', '2023-05-14'),
(855, 'Anuel', 'Martinez', 'Campos', 'Calle Margaritas #31 Col.Paraiso Cuautla Morelos', 7358964152, 'bbsita@gmail.com', '2023-05-14'),
(19680159, 'Anibal Eliud', 'Barrera', 'Morales', 'En lo mas jodido de todo CUAUTLA Morelos', 7358964512, 'anibalgay@gmail.com', '2023-04-30');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`id_pago`),
  ADD KEY `id_socio` (`id_socio`);

--
-- Indices de la tabla `sesion`
--
ALTER TABLE `sesion`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `socios`
--
ALTER TABLE `socios`
  ADD PRIMARY KEY (`id_socio`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pago`
--
ALTER TABLE `pago`
  MODIFY `id_pago` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pago`
--
ALTER TABLE `pago`
  ADD CONSTRAINT `pago_ibfk_1` FOREIGN KEY (`id_socio`) REFERENCES `socios` (`id_socio`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

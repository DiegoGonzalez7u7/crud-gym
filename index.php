<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Secular+One&display=swap" rel="stylesheet">
    <title>Iniciar Sesion</title>
</head>
<style>
    body {
        background-color: #88DFE2;
    }

    div.col-sm-6 {
        background-color: #FAF8ED;
        border-radius: 10px;
    }

    a {
        color: white;
    }

    h2 {
        font-family: 'Secular One', sans-serif;
    }
</style>

<body>

</body>
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <h2 class="text-center my-4">Iniciar sesión</h2>
            <form action="index.php" method="POST" onsubmit="return validarFormulario()">
                <div class="form-group">
                    <label for="username">ID de usuario:</label>
                    <input type="id" class="form-control" id="usuario" name="id_usuario" placeholder="Introduce tu nombre de usuario">
                </div>
                <div class="form-group">
                    <label for="password">Contraseña:</label>
                    <input type="password" class="form-control" id="pass" name="pass" placeholder="Introduce tu contraseña">
                </div>
                <div class="text-center">
                    <button class="btn btn-primary mr-3 mb-5" type="submit" id="miBoton">Iniciar sesion </button>
                    <button class="btn btn-secondary ml-3 mb-5"><a href="registro.html">Registrarse</a></button>
                </div>
            </form>
        </div>
    </div>
</div>

</html>

<?php
// Recibir los datos del formulario
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id_usuario = $_POST["id_usuario"];
    $password = $_POST["pass"];

    // Validar y verificar los datos ingresados
    if (empty($id_usuario) || empty($password)) {
        $error = "Por favor, ingresa un usuario y contraseña";
        header("Location: index.php");
        exit();
    }

    include("conexion.php");
    // Verificar si la conexión es exitosa
    if ($conn->connect_error) {
        die("Error en la conexión a la base de datos: " . $conn->connect_error);
    }

    // Consulta para verificar los datos ingresados
    $sql = "SELECT * FROM sesion WHERE id_usuario = '$id_usuario' AND contraseña = '$password'";
    $result = $conn->query($sql);
    //consulta del nombre del usuario que inicio sesion
    $sql2 = "SELECT nombre_usuario FROM sesion WHERE id_usuario = '$id_usuario'";
    //se hace la query
    $result2 = $conn->query($sql2);
    //almacenamos el resultado de la query y usamos el fetch porque es un objeto
    $nombre_usuario = $result2->fetch_assoc();
    if ($result->num_rows == 1) {
        // Iniciar sesión y redirigir al usuario a la página de inicio o área segura
        session_start();
        $_SESSION["nombre"] = $nombre_usuario["nombre_usuario"]; //aqui usamos la variable anterior y buscamos la data con el campo que requiramos
        header("Location: principal.php");  // Cambia "inicio.php" por la página a la que deseas redirigir al usuario
        exit();
    } else {
        $error = "Nombre de usuario o contraseña incorrectos";
        header("Location: index.php");
        exit();
    }

    $conn->close();
}
?>
<script>
    function validarFormulario() {
        var usuario = document.getElementById("usuario").value;
        var password = document.getElementById("pass").value;

        if (usuario === "") {
            alert("Por favor, ingresa un usuario");
            return false;
        }

        if (password === "") {
            alert("Por favor, ingresa una contraseña");
            return false;
        }

        // Si todas las validaciones pasan, el formulario se envía
        return true;
    }
</script>